class Report < ActiveRecord::Base
  validates :first_field, presence: true
  validates :second_field, presence: true
  validates :third_field, presence: true
end
