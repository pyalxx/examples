class Icon < ActiveRecord::Base
  require 'zip'
  has_attached_file :image,
                    path: ":rails_root/public/icons/:id/icon_:style.:extension",
                    url: "/icons/:id/icon_:style.:extension",
                    styles: {
                        :'72' => ["72x72#", :png],
                        :'100' => ["100x100#", :png],
                        :'144' => ["144x144#", :png]
                    },
                    default_url: "/images/:style/missing.png"
  validates_attachment :image, presence: true
  validates_attachment_content_type :image, content_type: ["image/jpg", "image/jpeg", "image/png"]

  def to_zip
    folder = "#{Rails.root}/public/icons/#{id}/"
    input_filenames = ['icon_72.png', 'icon_100.png', 'icon_144.png']
    archive_filename = "#{Rails.root}/public/icons/#{id}/icons.zip"

    Zip::File.open(archive_filename, Zip::File::CREATE) do |zipfile|
      input_filenames.each do |filename|
        zipfile.add(filename, folder + filename)
      end
    end
  end
end
