class FayeWrapper
  def self.publish(channel, message)
    run_event_machine
    FAYE[:client].publish(channel, message)
  end

  def self.run_event_machine
    Thread.new { EM.run } unless EM.reactor_running?
    Thread.pass until EM.reactor_running?
  end
end