class Message < ActiveRecord::Base

  validates :text, presence: true

  after_create :notify_subscribers

  def notify_subscribers
    FayeWrapper.publish('/messages/new', self.as_json)
  end

  def self.bulk_delete
    Message.delete_all
    messages = Message.all
    FayeWrapper.publish('/messages/all', messages.as_json)
    messages
  end
end
