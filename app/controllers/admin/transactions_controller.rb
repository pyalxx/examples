class Admin::TransactionsController < ApplicationController
  def index
    transactions = params[:account_id].nil? ? Transaction.all : Account.find(params[:account_id]).transactions
    render json: transactions
  end

  def update
    transaction = Transaction.find(params[:id])
    if transaction.update(permit_params)
      render json: transaction
    else
      render json: transaction.errors.full_messages, status: :unprocessable_entity
    end
  end

  private
  def permit_params
    params.require(:transaction).permit(:status)
  end
end
