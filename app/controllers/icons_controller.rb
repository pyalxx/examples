class IconsController < ApplicationController
  def new
  end

  def create
    icon = Icon.new(icon_params)
    if icon.save
      redirect_to icon_path(icon)
    else
      render 'new'
    end
  end

  def show
    @icon = Icon.find(params[:id])
    respond_to do |format|
      format.html
      format.zip do
        @icon.to_zip
        send_file "#{Rails.root}/public/icons/#{@icon.id}/icons.zip"
      end
    end
  end

  private
  def icon_params
    params.require(:icon).permit(:image)
  end
end
