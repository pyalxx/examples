class ChargeController < ApplicationController
  def index
    @charges = Charge.all
  end

  def new
  end

  def create
    # Amount in cents
    @amount = 500

    customer = Stripe::Customer.create(
        :email => params[:stripeEmail],
        :card => params[:stripeToken]
    )

    Stripe::Charge.create(
        :customer => customer.id,
        :amount => @amount,
        :currency => 'usd'
    )

    Charge.create(email: params[:stripeEmail], amount: @amount.to_s)

    redirect_to charge_index_path
  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to new_charge_path
  end
end
