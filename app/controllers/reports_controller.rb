class ReportsController < ApplicationController
  def index
    @reports = Report.all
  end

  def new
  end

  def create
    r = Report.new(report_params)
    if r.save
      redirect_to reports_path
    else
      @errors = r.errors.full_messages
      render 'new'
    end
  end

  def show
    @report = Report.find(params[:id])
    respond_to do |format|
      format.csv do
        headers['Content-Disposition'] = "attachment; filename=\"report-#{@report.id}.csv\""
        headers['Content-Type'] ||= 'text/csv'
      end
    end
  end

  private
  def report_params
    params.require(:report).permit(:first_field, :second_field, :third_field)
  end
end
