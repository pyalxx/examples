class CalendarsController < ApplicationController
  def index
    @calendar =RiCal.Calendar do |c|
      c.event do |e|
        e.summary = params[:calendar][:title]
        e.description = params[:calendar][:description]
        e.dtstart = DateTime.parse(params[:calendar][:start_date])
        e.dtend = DateTime.parse(params[:calendar][:end_date])
      end
    end
    respond_to do |format|
      format.ics { send_data(@calendar.export, filename: 'example.ics', disposition: 'inline; filename=example.ics', type: 'text/calendar') }
    end
  end

  def new
  end
end