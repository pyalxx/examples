class MessagesController < ApplicationController

  def index
    render json: Message.all
  end

  def create
    message = Message.new(message_params)
    if message.save
      render json: message
    else
      render json: {errors: message.errors.full_messages}, status: 422
    end
  end

  def chat
  end

  def clear
    messages = Message.bulk_delete
    render json: messages
  end

  private
  def message_params
    params.require(:message).permit(:text)
  end
end
