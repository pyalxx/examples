class DirectController < ApplicationController
  def index
    @user = TWITTER.user
    @messages = TWITTER.direct_messages_received
  end
end
