local_ip = Socket.ip_address_list.detect { |ip| ip.ipv4? && !(ip.ipv6? || ip.ipv4_loopback? || ip.ipv4_multicast? || ip.ipv4_private?) }
address = local_ip.nil? ? 'localhost' : local_ip.ip_address

FAYE = {
    address: address,
    client: Faye::Client.new("http://#{address}:9292/faye")
}

Thread.new do
  system('rackup faye.ru -E production -D')
end