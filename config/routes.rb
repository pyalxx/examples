Rails.application.routes.draw do

  namespace :admin do
    resources :accounts, only: [:index] do
      resources :transactions, only: [:index]
    end
    resources :transactions, only: [:index, :update]
  end
  resources :direct, only: [:index]

  resources :messages, only: [:index, :create] do
    collection do
      get :chat
      get :clear
    end
  end

  resources :ship2myid, only: [:index]

  resources :streaming, only: [:index, :create]

  resources :charge, only: [:new, :create, :index]

  resources :calendars, only: [:new] do
    collection do
      post :index
    end
  end

  resources :reports, only: [:new, :index, :create, :show]
  resources :icons, only: [:new, :create, :show]
  resources :todo, only: [:index]

  root to: 'streaming#index'
end
