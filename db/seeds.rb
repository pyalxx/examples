3.times do
  Account.create(email: Faker::Internet.email, phone_number: Faker::PhoneNumber.cell_phone)
end

10.times do
  Transaction.create(status: 'succeeded', amount: rand(100..10000).to_f/100, account_id: rand(1..3))
  sleep rand(1..3)
end