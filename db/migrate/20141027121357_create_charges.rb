class CreateCharges < ActiveRecord::Migration
  def change
    create_table :charges do |t|
      t.string :email
      t.string :amount

      t.timestamps
    end
  end
end
