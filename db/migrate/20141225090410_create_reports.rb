class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :first_field
      t.string :second_field
      t.string :third_field

      t.timestamps
    end
  end
end
